package com.artivisi.training.microservices201904.catalog.controller;

import com.artivisi.training.microservices201904.catalog.dao.ProdukDao;
import com.artivisi.training.microservices201904.catalog.entity.Produk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProdukController {

    @Autowired private ProdukDao produkDao;

    @GetMapping("/produk/")
    public Page<Produk> dataProduk(Pageable page) {
        return produkDao.findAll(page);
    }
}
